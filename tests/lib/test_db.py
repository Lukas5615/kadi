# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from datetime import datetime
from datetime import timezone

from kadi.lib.db import composite_index
from kadi.lib.db import get_class_by_tablename
from kadi.lib.db import get_class_of_relationship
from kadi.lib.db import get_column_type
from kadi.lib.db import is_column
from kadi.lib.db import is_many_relationship
from kadi.lib.db import is_relationship
from kadi.lib.db import length_constraint
from kadi.lib.db import range_constraint
from kadi.lib.db import values_constraint
from kadi.lib.resources.utils import add_link
from kadi.lib.resources.utils import remove_link
from kadi.modules.collections.models import Collection
from kadi.modules.records.models import Record


def test_utc_date_time(db, dummy_record):
    """Check if the custom UTCDateTime type for dates works correctly."""
    current_local_time = datetime.now()
    dummy_record.created_at = current_local_time
    db.session.commit()
    db.session.refresh(dummy_record)

    assert dummy_record.created_at == current_local_time.astimezone(timezone.utc)
    assert dummy_record.created_at.tzinfo == timezone.utc


def test_timestamp_mixin(db, dummy_collection, dummy_record, dummy_user):
    """Test if timestamps of database objects update correctly."""

    # Check if the timestamp gets updated when setting the state to deleted.
    prev_timestamp = dummy_record.last_modified
    dummy_record.state = "deleted"
    db.session.commit()

    assert dummy_record.last_modified != prev_timestamp

    # Check if the timestamp stays unchanged when the state is set to deleted.
    prev_timestamp = dummy_record.last_modified
    dummy_record.identifier = "test"
    db.session.commit()

    assert dummy_record.last_modified == prev_timestamp

    # Check if the timestamp also stays unchanged when trying to update it manually.
    dummy_record.update_timestamp()
    assert dummy_record.last_modified == prev_timestamp

    # Check if the timestamp gets updated again when setting the state to active.
    prev_timestamp = dummy_record.last_modified
    dummy_record.state = "active"
    db.session.commit()

    assert dummy_record.last_modified != prev_timestamp

    # Check if the timestamp gets updated when modifying a relationship.
    prev_timestamp = dummy_record.last_modified
    add_link(dummy_record.collections, dummy_collection, user=dummy_user)
    db.session.commit()

    assert dummy_record.last_modified != prev_timestamp

    # Check if the timestamp stays unchanged when modifying a relationship with a
    # deleted resource.
    prev_timestamp = dummy_record.last_modified
    dummy_collection.state = "deleted"
    remove_link(dummy_record.collections, dummy_collection, user=dummy_user)
    db.session.commit()

    assert dummy_record.last_modified == prev_timestamp


def test_length_constraint():
    """Test if length constraints are generated correctly."""
    constraint = length_constraint("test", min_value=0, max_value=1)

    assert constraint.name == "ck_test_length"
    assert (
        str(constraint.sqltext) == "char_length(test) >= 0 AND char_length(test) <= 1"
    )


def test_range_constraint():
    """Test if range constraints are generated correctly."""
    constraint = range_constraint("test", min_value=0, max_value=1)

    assert constraint.name == "ck_test_range"
    assert str(constraint.sqltext) == "test >= 0 AND test <= 1"


def test_values_constraint():
    """Test if value constraints are generated correctly."""
    constraint = values_constraint("test", ["test1", "test2"])

    assert constraint.name == "ck_test_values"
    assert str(constraint.sqltext) == "test IN ('test1', 'test2')"


def test_composite_index():
    """Test if composite indices are generated correctly."""
    assert composite_index("test1", "test2").name == "ix_test1_test2"


def test_get_class_by_tablename():
    """Test if determining a class by tablename works correctly."""
    assert get_class_by_tablename("record") == Record
    assert get_class_by_tablename("test") is None


def test_get_class_of_relationship():
    """Test if determining the class of a relationship works correctly."""
    assert get_class_of_relationship(Record, "collections") == Collection


def test_get_column_type(db):
    """Test if determining the column type of a model works correctly."""
    assert isinstance(get_column_type(Record, "identifier"), db.Text)


def test_is_column():
    """Test if columns of a model are determined correctly."""
    assert is_column(Record, "identifier")
    assert not is_column(Record, "collections")


def test_is_relationship():
    """Test if relationships of a model are determined correctly."""
    assert is_relationship(Record, "collections")
    assert not is_relationship(Record, "identifier")


def test_is_many_relationship():
    """Test if many-relationships of a model are determined correctly."""
    assert is_many_relationship(Record, "collections")
    assert not is_many_relationship(Record, "creator")
    assert not is_many_relationship(Record, "identifier")
