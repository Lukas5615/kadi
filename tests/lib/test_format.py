# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from datetime import datetime
from datetime import timezone

from kadi.lib.constants import ONE_DAY
from kadi.lib.constants import ONE_HOUR
from kadi.lib.constants import ONE_MINUTE
from kadi.lib.constants import ONE_WEEK
from kadi.lib.format import durationformat
from kadi.lib.format import pretty_type_name
from kadi.lib.format import timestamp
from kadi.modules.records.models import Record


def test_durationformat():
    """Test if durations are formatted correctly."""
    assert durationformat(0) == "0 seconds"
    assert durationformat(1) == "1 second"
    assert durationformat(ONE_MINUTE) == "1 minute"
    assert durationformat(ONE_HOUR) == "1 hour"
    assert durationformat(ONE_DAY) == "1 day"
    assert durationformat(ONE_WEEK) == "1 week"
    assert (
        durationformat(ONE_WEEK + ONE_DAY + ONE_HOUR + ONE_MINUTE + 1)
        == "1 week, 1 day, 1 hour, 1 minute, 1 second"
    )


def test_pretty_type_name():
    """Test if type names are prettified correctly."""
    assert pretty_type_name("str") == "string"
    assert pretty_type_name(str) == "string"
    assert pretty_type_name("Record") == "Record"
    assert pretty_type_name(Record) == "Record"


def test_timestamp():
    """Test if timestamps are generated correctly."""
    date_time = datetime.strptime(
        "2020-01-01T00:00:00.000Z", "%Y-%m-%dT%H:%M:%S.%fZ"
    ).replace(tzinfo=timezone.utc)

    assert timestamp(date_time=date_time) == "20200101000000"
    assert timestamp(date_time=date_time, include_micro=True) == "20200101000000000000"
