# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from datetime import timezone

import pytest
from werkzeug.datastructures import MultiDict
from wtforms import StringField
from wtforms.validators import StopValidation

from kadi.lib.forms import check_duplicate_identifier
from kadi.lib.forms import DynamicMultiSelectField
from kadi.lib.forms import DynamicSelectField
from kadi.lib.forms import KadiForm
from kadi.lib.forms import LFTextAreaField
from kadi.lib.forms import TagsField
from kadi.lib.forms import UTCDateTimeField
from kadi.lib.utils import named_tuple
from kadi.modules.records.models import Record


def test_kadi_form():
    """Test if the custom form base class works correctly."""

    class _TestForm(KadiForm):
        test = StringField()

    form = _TestForm()
    assert form.test.id == form.test.label.field_id == "test"

    form = _TestForm(_suffix="suffix")
    assert form.test.label.field_id == form.test.label.field_id == "test_suffix"


def test_dynamic_select_field():
    """Test if the custom "DynamicSelectField" works correctly."""

    class _TestForm(KadiForm):
        test = DynamicSelectField("Test", coerce=int)

    form = _TestForm(formdata=MultiDict({"test": "1"}))

    assert form.validate()
    assert form.test.data == 1

    form = _TestForm(formdata=MultiDict({"test": "test"}))

    assert not form.validate()
    assert form.test.data is None


def test_dynamic_multi_select_field():
    """Test if the custom "DynamicMultiSelectField" works correctly."""

    class _TestForm(KadiForm):
        test = DynamicMultiSelectField("Test", coerce=int)

    form = _TestForm(formdata=MultiDict({"test": ["1", "2"]}))

    assert form.validate()
    assert form.test.data == [1, 2]

    form = _TestForm(formdata=MultiDict({"test": ["1", "test"]}))

    assert not form.validate()
    assert form.test.data == []


def test_tags_field():
    """Test if the custom "TagsField" works correctly."""
    max_len = 3

    class _TestForm(KadiForm):
        test = TagsField("Test", max_len=max_len)

    form = _TestForm(formdata=MultiDict({"test": [" A ", "a", "a   b"]}))

    assert form.validate()
    assert form.test.data == ["a", "a b"]

    form = _TestForm(formdata=MultiDict({"test": ["abcd"]}))

    assert not form.validate()
    assert f"Tags cannot be longer than {max_len} characters." in form.errors["test"]

    form = _TestForm(formdata=MultiDict({"test": [" "]}))

    assert not form.validate()
    assert "Tags must not be empty." in form.errors["test"]


def test_lf_text_area_field():
    """Test if the custom "LFTextAreaField" works correctly."""

    class _TestForm(KadiForm):
        test = LFTextAreaField("Test")

    form = _TestForm(formdata=MultiDict({"test": "a\r\nb"}))

    assert form.validate()
    assert form.test.data == "a\nb"


def test_utc_date_time_field():
    """Test if the custom "UTCDateTimeField" works correctly."""

    class _TestForm(KadiForm):
        test = UTCDateTimeField("Test")

    form = _TestForm(formdata=MultiDict({"test": "2020-01-01T00:00:00.000Z"}))

    assert form.validate()
    assert form.test.data.tzinfo == timezone.utc


def test_check_duplicate_identifier(dummy_record):
    """Test if checking for a duplicate identifier works correctly."""
    check_duplicate_identifier(named_tuple("Field", data="test"), Record)
    check_duplicate_identifier(
        named_tuple("Field", data=dummy_record.identifier), Record, exclude=dummy_record
    )

    with pytest.raises(StopValidation):
        check_duplicate_identifier(
            named_tuple("Field", data=dummy_record.identifier), Record
        )
