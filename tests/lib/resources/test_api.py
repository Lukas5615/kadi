# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.resources.api import add_link
from kadi.lib.resources.api import add_role
from kadi.lib.resources.api import change_role
from kadi.lib.resources.api import remove_link
from kadi.lib.resources.api import remove_role
from tests.utils import check_api_response


def test_add_link(dummy_collection, dummy_record, dummy_user, new_user):
    """Test if adding links via the API works correctly."""
    response = add_link(dummy_record.collections, dummy_collection, user=new_user())

    check_api_response(response, status_code=403)
    assert not dummy_record.collections.all()

    response = add_link(dummy_record.collections, dummy_collection, user=dummy_user)

    check_api_response(response, status_code=201)
    assert dummy_record.collections.one().id == dummy_collection.id

    response = add_link(dummy_record.collections, dummy_collection, user=dummy_user)

    check_api_response(response, status_code=409)
    assert dummy_record.collections.one().id == dummy_collection.id


def test_remove_link(dummy_collection, dummy_record, dummy_user, new_user):
    """Test if removing links via the API works correctly."""
    response = remove_link(dummy_record.collections, dummy_collection, user=new_user())

    check_api_response(response, status_code=403)
    assert not dummy_record.collections.all()

    response = remove_link(dummy_record.collections, dummy_collection, user=dummy_user)

    check_api_response(response, status_code=404)
    assert not dummy_record.collections.all()

    add_link(dummy_record.collections, dummy_collection, user=dummy_user)
    response = remove_link(dummy_record.collections, dummy_collection, user=dummy_user)

    check_api_response(response, status_code=204)
    assert not dummy_record.collections.all()


def test_add_role(dummy_collection, dummy_record, new_user):
    """Test if adding roles via the API works correctly."""
    user = new_user()
    object_name = "record"

    response = add_role(user, dummy_record, "test")

    check_api_response(response, status_code=400)
    assert not user.roles.filter_by(object=object_name).all()

    response = add_role(user, dummy_record, "member")

    check_api_response(response, status_code=201)
    assert user.roles.filter_by(object=object_name).one().name == "member"

    response = add_role(user, dummy_record, "member")

    check_api_response(response, status_code=409)
    assert user.roles.filter_by(object=object_name).one().name == "member"


def test_remove_role(dummy_collection, dummy_record, dummy_user, new_user):
    """Test if removing roles via the API works correctly."""
    user = new_user()
    object_name = "record"

    response = remove_role(dummy_user, dummy_record)

    check_api_response(response, status_code=409)
    assert dummy_user.roles.filter_by(object=object_name).one().name == "admin"

    response = remove_role(user, dummy_record)

    check_api_response(response, status_code=404)
    assert not user.roles.filter_by(object=object_name).all()

    add_role(user, dummy_record, "member")
    response = remove_role(user, dummy_record)

    check_api_response(response, status_code=204)
    assert not user.roles.filter_by(object=object_name).all()


def test_change_role(db, dummy_collection, dummy_record, dummy_user, new_user):
    """Test if changing roles via the API works correctly."""
    user = new_user()
    object_name = "record"

    response = change_role(dummy_user, dummy_record, "member")

    check_api_response(response, status_code=409)
    assert dummy_user.roles.filter_by(object=object_name).one().name == "admin"
    response = change_role(user, dummy_record, "member")

    check_api_response(response, status_code=404)
    assert not user.roles.filter_by(object=object_name).all()

    add_role(user, dummy_record, "member")
    response = change_role(user, dummy_record, "editor")

    check_api_response(response, status_code=200)
    assert user.roles.filter_by(object=object_name).one().name == "editor"

    # Start a new savepoint, so only the latest changes get rolled back.
    db.session.begin_nested()
    response = change_role(user, dummy_record, "test")
    db.session.rollback()

    check_api_response(response, status_code=400)
    assert user.roles.filter_by(object=object_name).one().name == "editor"
