# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest

from kadi.lib.exceptions import KadiPermissionError
from kadi.lib.resources.utils import add_link
from kadi.lib.resources.utils import get_linked_resources
from kadi.lib.resources.utils import remove_link
from kadi.modules.collections.models import Collection
from kadi.modules.permissions.utils import add_role


def test_add_link(dummy_collection, dummy_record, dummy_user, new_user):
    """Test if linking two resources works correctly."""
    user = new_user()

    with pytest.raises(KadiPermissionError):
        add_link(dummy_record.collections, dummy_collection, user=user)

    assert add_link(dummy_record.collections, dummy_collection, user=dummy_user)
    assert dummy_record.collections[0].id == dummy_collection.id
    # This is essentially the same link as before, so it is expected to fail.
    assert not add_link(dummy_collection.records, dummy_record, user=dummy_user)


def test_remove_link(dummy_collection, dummy_record, dummy_user, new_user):
    """Test if unlinking two resources works correctly."""
    user = new_user()
    add_link(dummy_record.collections, dummy_collection, user=dummy_user)

    with pytest.raises(KadiPermissionError):
        remove_link(dummy_record.collections, dummy_collection, user=user)

    assert remove_link(dummy_record.collections, dummy_collection, user=dummy_user)
    assert not dummy_record.collections.all()
    # The second call is expected to fail if the link was removed successfully.
    assert not remove_link(dummy_record.collections, dummy_collection, user=dummy_user)


def test_get_linked_resources(
    dummy_collection, dummy_record, dummy_user, new_user, user_context
):
    """Test if determining linked resources works correctly."""
    user = new_user()
    add_link(dummy_record.collections, dummy_collection, user=dummy_user)

    # Check the linked resources the resources creator can obtain.
    linked_resources = get_linked_resources(
        Collection, dummy_record.collections, user=dummy_user
    )
    assert linked_resources.one().id == dummy_collection.id

    linked_resources = get_linked_resources(
        Collection, dummy_record.collections, user=dummy_user, actions=["link"]
    )
    assert linked_resources.one().id == dummy_collection.id

    # Check the resources if the given actions are invalid.
    linked_resources = get_linked_resources(
        Collection, dummy_record.collections, user=dummy_user, actions=["invalid"]
    )
    assert not linked_resources.all()

    # Check the resources a permitted user can obtain if they can read but not link the
    # resource.
    add_role(user, "collection", dummy_collection.id, "member")

    linked_resources = get_linked_resources(
        Collection, dummy_record.collections, user=user
    )
    assert linked_resources.one().id == dummy_collection.id

    linked_resources = get_linked_resources(
        Collection, dummy_record.collections, user=user, actions=["link"]
    )
    assert not linked_resources.all()
