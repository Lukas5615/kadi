# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest
from flask import current_app

from kadi.lib.web import url_for
from kadi.modules.accounts.utils import save_user_image
from tests.utils import check_api_response


def test_preview_user_image(
    monkeypatch, tmp_path, client, dummy_image, dummy_user, user_session
):
    """Test the internal "api.preview_user_image" endpoint."""
    monkeypatch.setitem(current_app.config, "MISC_UPLOADS_PATH", tmp_path)

    save_user_image(dummy_user, dummy_image)

    with user_session():
        response = client.get(url_for("api.preview_user_image", id=dummy_user.id))
        check_api_response(response, content_type="image/jpeg")


def test_select_users(client, user_session):
    """Test the internal "api.select_users" endpoint."""
    with user_session():
        response = client.get(url_for("api.select_users"))
        check_api_response(response)


def test_get_users(api_client, dummy_access_token):
    """Test the "api.get_users" endpoint."""
    response = api_client(dummy_access_token).get(url_for("api.get_users"))
    check_api_response(response)


def test_get_current_user(api_client, dummy_access_token, dummy_user):
    """Test the "api.get_current_user" endpoint."""
    response = api_client(dummy_access_token).get(url_for("api.get_current_user"))

    check_api_response(response)
    assert response.get_json()["id"] == dummy_user.id


@pytest.mark.parametrize(
    "endpoint,args",
    [
        ("get_user", {}),
        ("get_user_identities", {}),
        ("get_user_groups", {}),
        ("get_user_records", {}),
        ("get_user_records", {"shared": True}),
        ("get_user_collections", {}),
        ("get_user_collections", {"shared": True}),
        ("get_user_templates", {}),
        ("get_user_templates", {"shared": True}),
    ],
)
def test_get_user_endpoints(endpoint, args, api_client, dummy_access_token, dummy_user):
    """Test the remaining "api.get_user*" endpoints."""
    response = api_client(dummy_access_token).get(
        url_for(f"api.{endpoint}", id=dummy_user.id, **args)
    )
    check_api_response(response)
