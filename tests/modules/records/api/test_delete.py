# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.resources.utils import add_link
from kadi.lib.web import url_for
from kadi.modules.records.models import RecordLink
from kadi.modules.records.models import Upload
from tests.modules.records.utils import initiate_upload
from tests.modules.resources import check_api_delete_subject_resource_role
from tests.utils import check_api_response


def test_delete_record(api_client, dummy_access_token, dummy_record):
    """Test the "api.delete_record" endpoint."""
    response = api_client(dummy_access_token).delete(
        url_for("api.delete_record", id=dummy_record.id)
    )

    check_api_response(response, status_code=204)
    assert dummy_record.state == "deleted"


def test_remove_record_link(
    api_client, db, dummy_access_token, dummy_record, new_record
):
    """Test the "api.remove_record_link" endpoint."""
    record = new_record()
    record_link = RecordLink.create(
        name="test", record_from=dummy_record, record_to=record
    )
    db.session.flush()

    response = api_client(dummy_access_token).delete(
        url_for(
            "api.remove_record_link", record_id=dummy_record.id, link_id=record_link.id
        )
    )

    check_api_response(response, status_code=204)
    assert not RecordLink.query.all()


def test_remove_record_collection(
    api_client, dummy_access_token, dummy_collection, dummy_record, dummy_user
):
    """Test the "api.remove_record_collection" endpoint."""
    add_link(dummy_record.collections, dummy_collection, user=dummy_user)

    response = api_client(dummy_access_token).delete(
        url_for(
            "api.remove_record_collection",
            record_id=dummy_record.id,
            collection_id=dummy_collection.id,
        )
    )

    check_api_response(response, status_code=204)
    assert not dummy_record.collections.all()


def test_remove_record_user_role(
    api_client, dummy_access_token, dummy_record, dummy_user, new_user
):
    """Test the "api.remove_record_user_role" endpoint."""
    user = new_user()

    check_api_delete_subject_resource_role(
        api_client(dummy_access_token),
        url_for(
            "api.remove_record_user_role", record_id=dummy_record.id, user_id=user.id
        ),
        user,
        dummy_record,
        remove_creator_endpoint=url_for(
            "api.remove_record_user_role",
            record_id=dummy_record.id,
            user_id=dummy_user.id,
        ),
    )


def test_remove_record_group_role(
    api_client, dummy_access_token, dummy_record, dummy_group
):
    """Test the "api.remove_record_group_role" endpoint."""
    check_api_delete_subject_resource_role(
        api_client(dummy_access_token),
        url_for(
            "api.remove_record_group_role",
            record_id=dummy_record.id,
            group_id=dummy_group.id,
        ),
        dummy_group,
        dummy_record,
    )


def test_delete_file(api_client, dummy_access_token, dummy_file, dummy_record):
    """Test the "api.delete_file" endpoint."""
    response = api_client(dummy_access_token).delete(
        url_for("api.delete_file", record_id=dummy_record.id, file_id=dummy_file.id)
    )

    check_api_response(response, status_code=204)
    assert dummy_file.state == "inactive"


def test_delete_upload(api_client, dummy_access_token, dummy_record):
    """Test the "api.delete_upload" endpoint."""
    client = api_client(dummy_access_token)
    response = initiate_upload(client, url_for("api.new_upload", id=dummy_record.id))
    upload_id = response.get_json()["id"]

    response = client.delete(
        url_for("api.delete_upload", record_id=dummy_record.id, upload_id=upload_id)
    )

    check_api_response(response, status_code=204)
    assert Upload.query.get(upload_id).state == "inactive"
