# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest
from flask import current_app

from kadi.lib.web import url_for
from kadi.modules.records.core import delete_record
from kadi.modules.records.models import Record
from tests.modules.records.utils import initiate_upload
from tests.modules.records.utils import upload_chunk
from tests.modules.resources import check_api_post_subject_resource_role
from tests.utils import check_api_response


def test_download_record_files(
    client, dummy_file, dummy_record, dummy_user, package_files_task, user_session
):
    """Test the internal "api.download_record_files" endpoint."""
    with user_session():
        response = client.post(url_for("api.download_record_files", id=dummy_record.id))
        check_api_response(response, status_code=202)


def test_restore_record(client, dummy_record, user_session):
    """Test the internal "api.restore_record" endpoint."""
    with user_session():
        delete_record(dummy_record)
        response = client.post(url_for("api.restore_record", id=dummy_record.id))

        check_api_response(response)
        assert dummy_record.state == "active"


def test_purge_record(client, dummy_record, user_session):
    """Test the internal "api.purge_record" endpoint."""
    with user_session():
        delete_record(dummy_record)
        response = client.post(url_for("api.purge_record", id=dummy_record.id))

        check_api_response(response, status_code=202)
        assert Record.query.get(dummy_record.id) is None


def test_new_record(api_client, dummy_access_token):
    """Test the "api.new_record" endpoint."""
    response = api_client(dummy_access_token).post(
        url_for("api.new_record"), json={"identifier": "test", "title": "test"}
    )

    check_api_response(response, status_code=201)
    assert Record.query.filter_by(identifier="test").first() is not None


def test_add_record_link(api_client, dummy_access_token, dummy_record, new_record):
    """Test the "api.add_record_link" endpoint."""
    record = new_record()

    response = api_client(dummy_access_token).post(
        url_for("api.add_record_link", id=dummy_record.id),
        json={"name": "test", "record_to": {"id": record.id}},
    )

    check_api_response(response, status_code=201)
    assert dummy_record.links_to.filter_by(name="test").first() is not None


def test_add_record_collection(
    api_client, dummy_access_token, dummy_collection, dummy_record
):
    """Test the "api.add_record_collection" endpoint."""
    response = api_client(dummy_access_token).post(
        url_for("api.add_record_collection", id=dummy_record.id),
        json={"id": dummy_collection.id},
    )

    check_api_response(response, status_code=201)
    assert dummy_record.collections[0].id == dummy_collection.id


def test_add_record_user_role(api_client, dummy_access_token, dummy_record, new_user):
    """Test the "api.add_record_user_role" endpoint."""
    check_api_post_subject_resource_role(
        api_client(dummy_access_token),
        url_for("api.add_record_user_role", id=dummy_record.id),
        new_user(),
        dummy_record,
    )


def test_add_record_group_role(
    api_client, dummy_access_token, dummy_record, dummy_group
):
    """Test the "api.add_record_group_role" endpoint."""
    check_api_post_subject_resource_role(
        api_client(dummy_access_token),
        url_for("api.add_record_group_role", id=dummy_record.id),
        dummy_group,
        dummy_record,
    )


def test_new_upload_success(api_client, dummy_access_token, dummy_record):
    """Test the success behaviour of the "api.new_upload" endpoint."""
    response = initiate_upload(
        api_client(dummy_access_token), url_for("api.new_upload", id=dummy_record.id)
    )
    check_api_response(response, status_code=201)


@pytest.mark.parametrize(
    "filename,conf_key,error_status",
    [("test.txt", "MAX_UPLOAD_SIZE", 413), ("test.txt", "MAX_UPLOAD_USER_QUOTA", 413)],
)
def test_new_upload_error(
    filename,
    conf_key,
    error_status,
    monkeypatch,
    tmp_path,
    api_client,
    dummy_access_token,
    dummy_record,
):
    """Test the error behaviour of the "api.new_upload" endpoint."""
    monkeypatch.setitem(current_app.config, "STORAGE_PATH", tmp_path)
    monkeypatch.setitem(current_app.config, conf_key, 10)

    response = initiate_upload(
        api_client(dummy_access_token),
        url_for("api.new_upload", id=dummy_record.id),
        name=filename,
        file_data=11 * b"x",
    )
    check_api_response(response, status_code=error_status)


def test_finish_upload_success(
    monkeypatch, tmp_path, api_client, dummy_access_token, dummy_record
):
    """Test the success behaviour of the "api.finish_upload" endpoint."""
    monkeypatch.setitem(current_app.config, "STORAGE_PATH", tmp_path)

    client = api_client(dummy_access_token)

    data = initiate_upload(
        client, url_for("api.new_upload", id=dummy_record.id)
    ).get_json()
    upload_chunk(client, data["_actions"]["upload_chunk"])

    response = client.post(data["_actions"]["finish_upload"])
    check_api_response(response, status_code=202)

    client = api_client(dummy_access_token)


@pytest.mark.parametrize(
    "file_data,error_description",
    [
        (11 * b"x", "Number of chunks does not match expected chunk count."),
        (b"x", "Total chunk size does not match upload size."),
    ],
)
def test_finish_upload_error(
    file_data,
    error_description,
    monkeypatch,
    tmp_path,
    api_client,
    dummy_access_token,
    dummy_record,
    new_file,
):
    """Test the error behaviour of the "api.finish_upload" endpoint."""
    monkeypatch.setitem(current_app.config, "STORAGE_PATH", tmp_path)
    monkeypatch.setitem(current_app.config, "MAX_UPLOAD_USER_QUOTA", 10)

    client = api_client(dummy_access_token)

    data = initiate_upload(
        client, url_for("api.new_upload", id=dummy_record.id), file_data=10 * b"x"
    ).get_json()
    response = upload_chunk(
        client, data["_actions"]["upload_chunk"], chunk_data=file_data
    )

    response = client.post(data["_actions"]["finish_upload"])

    check_api_response(response, status_code=400)
    assert error_description == response.get_json()["description"]
