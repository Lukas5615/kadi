# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import os
import zipfile
from io import BytesIO

from flask import current_app

from kadi.lib.storage.core import create_filepath
from kadi.lib.storage.local import LocalStorage
from kadi.modules.records.files import delete_file
from kadi.modules.records.files import get_archive_contents
from kadi.modules.records.files import get_preview_type
from kadi.modules.records.files import get_text_contents
from kadi.modules.records.files import open_file
from kadi.modules.records.files import package_files
from kadi.modules.records.files import remove_files
from kadi.modules.records.files import remove_temporary_files
from kadi.modules.records.files import update_file
from kadi.modules.records.models import File
from kadi.modules.records.models import TemporaryFile
from kadi.modules.records.models import Upload
from kadi.modules.records.uploads import save_chunk


def test_update_file(dummy_file, dummy_record, user_context):
    """Test if updating files works correctly."""
    prev_timestamp = dummy_record.last_modified

    with user_context():
        update_file(dummy_file, name="test.txt")

        assert dummy_file.name == "test.txt"
        assert dummy_file.revisions.count() == 2
        assert dummy_record.last_modified != prev_timestamp


def test_delete_file(dummy_file, dummy_record, new_upload, user_context):
    """Test if deleting files works correctly."""
    prev_timestamp = dummy_record.last_modified
    upload = new_upload(file=dummy_file)

    with user_context():
        delete_file(dummy_file)

        assert dummy_file.state == "inactive"
        assert dummy_file.revisions.count() == 2
        assert dummy_record.last_modified != prev_timestamp
        assert upload.state == "inactive"


def test_remove_files(monkeypatch, tmp_path, new_file, new_upload):
    """Test if removing files works correctly."""
    monkeypatch.setitem(current_app.config, "STORAGE_PATH", tmp_path)

    file_data = 10 * b"x"

    file = new_file()
    upload = new_upload(file=file, size=len(file_data))
    save_chunk(
        upload=upload, file_object=BytesIO(file_data), index=0, size=len(file_data)
    )

    remove_files(file, delete_from_db=False)

    assert file.state == "deleted"
    assert not Upload.query.all()
    assert not os.listdir(tmp_path)

    remove_files(file, delete_from_db=True)

    assert not File.query.all()


def test_open_file(dummy_file, dummy_image):
    """Test if opening files works correctly."""
    with open_file(dummy_file) as f:
        assert f.read() == dummy_image.getvalue()


def test_get_preview_type(dummy_file, new_file):
    """Test if determining the preview type of files works correctly."""
    text_file = new_file(file_data=10 * b"x")
    empty_file = new_file(file_data=b"")

    assert get_preview_type(dummy_file) == "image"
    assert get_preview_type(text_file) == "text;ascii"
    assert get_preview_type(empty_file) is None


def test_get_archive_contents(monkeypatch, tmp_path, new_file):
    """Test if determining the contents of an archive works correctly."""
    monkeypatch.setitem(current_app.config, "STORAGE_PATH", tmp_path)

    file_content = "test"
    zip_buffer = BytesIO()

    with zipfile.ZipFile(
        zip_buffer, mode="w", compression=zipfile.ZIP_DEFLATED
    ) as zip_file:
        zip_file.writestr(file_content, file_content)

    file = new_file(file_data=zip_buffer.getvalue())

    assert get_archive_contents(file) == [
        {"name": file_content, "size": len(file_content), "is_dir": False}
    ]


def test_get_text_contents(monkeypatch, tmp_path, new_file):
    """Test if determining the contents of an archive works correctly."""
    monkeypatch.setitem(current_app.config, "STORAGE_PATH", tmp_path)

    file_content = "test"
    file = new_file(file_data=file_content.encode())

    assert get_text_contents(file) == file_content


def test_package_files(dummy_file, dummy_record, dummy_user):
    """Test if packaging files works correctly."""
    temporary_file = package_files(dummy_record, dummy_user)
    filepath = create_filepath(str(temporary_file.id))

    with zipfile.ZipFile(filepath) as zip_file:
        entry = zip_file.infolist()[0]

        assert entry.filename == dummy_file.name
        assert entry.file_size == dummy_file.size


def test_remove_temporary_files(monkeypatch, tmp_path, db, dummy_record, dummy_user):
    """Test if removing temporary files works correctly."""
    monkeypatch.setitem(current_app.config, "STORAGE_PATH", tmp_path)

    file_data = 10 * b"x"

    temporary_file = TemporaryFile.create(
        record=dummy_record,
        creator=dummy_user,
        name="test.txt",
        size=len(file_data),
        state="active",
    )
    db.session.flush()

    dst = create_filepath(str(temporary_file.id))
    LocalStorage().save(dst, BytesIO(file_data))

    remove_temporary_files(temporary_file)

    assert not TemporaryFile.query.all()
    assert not os.listdir(tmp_path)
