# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest
from flask import current_app

from kadi.lib.db import get_class_by_tablename
from kadi.modules.permissions.core import get_permitted_objects
from kadi.modules.permissions.core import has_permission
from kadi.modules.permissions.models import Permission
from kadi.modules.permissions.models import Role
from kadi.modules.permissions.utils import add_role
from kadi.modules.permissions.utils import delete_permissions
from kadi.modules.permissions.utils import get_group_roles
from kadi.modules.permissions.utils import get_user_roles
from kadi.modules.permissions.utils import remove_role


def test_system_role_permissions(new_user):
    """Test the permissions of system roles."""
    for role_name, permissions in current_app.config["SYSTEM_ROLES"].items():
        user = new_user(system_role=role_name)

        # Check if the user has only the correct global permissions.
        for object_name, global_actions in permissions.items():
            model = get_class_by_tablename(object_name)

            for action, _ in model.Meta.permissions["global_actions"]:
                user_has_permission = has_permission(user, action, object_name, None)

                if action in global_actions:
                    assert user_has_permission
                else:
                    assert not user_has_permission


@pytest.mark.parametrize(
    "fixture", ["dummy_record", "dummy_collection", "dummy_group", "dummy_template"]
)
def test_default_resource_creator_permissions(fixture, getfixture, dummy_user):
    """Test the default permissions of created resources."""
    resource = getfixture(fixture)
    model = resource.__class__
    object_name = model.__tablename__

    # Check if the creator has permissions (or a suitable role) allowing him to perform
    # any action.
    for action, _ in model.Meta.permissions["actions"]:
        assert has_permission(dummy_user, action, object_name, resource.id)
        assert resource in get_permitted_objects(dummy_user, action, object_name)


def test_delete_permissions(dummy_record):
    """Test if deleting permissions works correctly."""
    delete_permissions("record", dummy_record.id)

    assert not Permission.query.filter_by(
        object="record", object_id=dummy_record.id
    ).all()
    assert not Role.query.filter_by(object="record", object_id=dummy_record.id).all()


def test_add_role(dummy_record, new_user):
    """Test if adding roles works correctly."""
    user = new_user()
    test_role = "member"
    prev_timestamp = dummy_record.last_modified

    role_added = add_role(user, "record", dummy_record.id + 1, test_role)
    assert not role_added

    with pytest.raises(ValueError):
        assert add_role(user, "record", dummy_record.id, "invalid")

    role_added = add_role(user, "record", dummy_record.id, test_role)
    assert role_added
    assert dummy_record.last_modified != prev_timestamp
    assert (
        user.roles.filter_by(
            object="record", object_id=dummy_record.id, name=test_role
        ).first()
        is not None
    )

    role_added = add_role(user, "record", dummy_record.id, test_role)
    assert not role_added


def test_remove_role(dummy_record, new_user):
    """Test if removing roles works correctly."""
    user = new_user()
    test_role = "member"
    prev_timestamp = dummy_record.last_modified

    role_removed = remove_role(user, "record", dummy_record.id, test_role)
    assert not role_removed

    add_role(user, "record", dummy_record.id, test_role)

    role_removed = remove_role(user, "record", dummy_record.id + 1, test_role)
    assert not role_removed

    role_removed = remove_role(user, "record", dummy_record.id, test_role)
    assert role_removed
    assert dummy_record.last_modified != prev_timestamp
    assert (
        user.roles.filter_by(object="record", object_id=dummy_record.id).first() is None
    )


def test_get_user_roles(dummy_record, dummy_user):
    """Test if determining user roles works correctly."""
    user_role = get_user_roles("record").first()

    assert user_role[0].id == dummy_user.id
    assert user_role[1].object_id == dummy_record.id

    user_role = get_user_roles("record", object_id=dummy_record.id).first()

    assert user_role[0].id == dummy_user.id
    assert user_role[1].object_id == dummy_record.id


def test_get_group_roles(dummy_group, dummy_record):
    """Test if determining user roles works correctly."""
    add_role(dummy_group, "record", dummy_record.id, "member")

    group_role = get_group_roles("record").first()

    assert group_role[0].id == dummy_group.id
    assert group_role[1].object_id == dummy_record.id

    group_role = get_group_roles("record", object_id=dummy_record.id).first()

    assert group_role[0].id == dummy_group.id
    assert group_role[1].object_id == dummy_record.id
