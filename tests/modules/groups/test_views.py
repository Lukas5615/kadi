# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.web import url_for
from kadi.modules.groups.models import Group
from tests.utils import check_view_response


def test_groups(client, user_session):
    """Test the "groups.groups" endpoint."""
    with user_session():
        response = client.get(url_for("groups.groups"))
        check_view_response(response)


def test_new_group(client, user_session):
    """Test the "groups.new_group" endpoint."""
    endpoint = url_for("groups.new_group")

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(
            endpoint,
            data={"identifier": "test", "title": "test", "visibility": "private"},
        )

        check_view_response(response, status_code=302)
        assert Group.query.filter_by(identifier="test").first() is not None


def test_edit_group(client, dummy_group, user_session):
    """Test the "groups.edit_group" endpoint."""
    endpoint = url_for("groups.edit_group", id=dummy_group.id)

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(endpoint, data={"identifier": "test"})

        check_view_response(response, status_code=302)
        assert dummy_group.identifier == "test"


def test_view_group(client, dummy_group, user_session):
    """Test the "groups.view_group" endpoint."""
    with user_session():
        response = client.get(url_for("groups.view_group", id=dummy_group.id))
        check_view_response(response)


def test_manage_members(client, dummy_group, new_user, user_session):
    """Test the "groups.manage_members" endpoint."""
    endpoint = url_for("groups.manage_members", id=dummy_group.id)
    new_role = "member"
    user = new_user()

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(endpoint, data={"users": [user.id], "role": "member"})

        check_view_response(response)
        assert (
            user.roles.filter_by(
                object="group", object_id=dummy_group.id, name=new_role
            ).first()
            is not None
        )


def test_view_revision(client, dummy_group, user_session):
    """Test the "groups.view_revision" endpoint."""
    with user_session():
        response = client.get(
            url_for(
                "groups.view_revision",
                group_id=dummy_group.id,
                revision_id=dummy_group.revisions[0].id,
            )
        )
        check_view_response(response)


def test_delete_group(client, dummy_group, user_session):
    """Test the "groups.delete_group" endpoint."""
    with user_session():
        response = client.post(url_for("groups.delete_group", id=dummy_group.id))

        check_view_response(response, status_code=302)
        assert dummy_group.state == "deleted"
