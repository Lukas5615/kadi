# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.modules.templates.schemas import TemplateSchema


def test_template_schema_post_load_record():
    """Test if loading "record" data in the "TemplateSchema" works correctly."""
    default_data = {
        "title": "",
        "identifier": "",
        "type": None,
        "description": "",
        "tags": [],
        "extras": [],
    }

    schema = TemplateSchema(only=["data", "type"])
    assert schema.load({"data": {}, "type": "record"})["data"] == default_data

    schema = TemplateSchema(only=["data"], type="record")
    assert schema.load({"data": {"title": "test"}})["data"] == {
        **default_data,
        "title": "test",
    }


def test_template_schema_post_load_extras():
    """Test if loading "extras" data in the "TemplateSchema" works correctly."""
    schema = TemplateSchema(only=["data", "type"])
    assert schema.load({"data": [], "type": "extras"})["data"] == []

    schema = TemplateSchema(only=["data"], type="extras")
    assert schema.load({"data": [{"key": "test", "type": "str"}]})["data"] == [
        {"key": "test", "type": "str", "value": None}
    ]
