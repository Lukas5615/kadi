# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest

from kadi.modules.templates.core import create_template
from kadi.modules.templates.core import delete_template
from kadi.modules.templates.core import update_template
from kadi.modules.templates.models import Template


@pytest.mark.parametrize("type,data", [("record", {}), ("extras", [])])
def test_create_template(type, data, dummy_user):
    """Test if templates are created correctly."""
    template = create_template(
        creator=dummy_user, identifier="test", title="test", type=type, data=data
    )
    assert Template.query.filter_by(identifier="test").first().id == template.id


def test_update_template(dummy_template, user_context):
    """Test if templates are updated correctly."""
    with user_context():
        update_template(dummy_template, identifier="test")
        assert dummy_template.identifier == "test"


def test_delete_template(db, dummy_template, user_context):
    """Test if templates are deleted correctly."""
    with user_context():
        delete_template(dummy_template)
        db.session.commit()

        assert not Template.query.all()
