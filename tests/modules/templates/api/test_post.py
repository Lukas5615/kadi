# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest

from kadi.lib.web import url_for
from kadi.modules.templates.models import Template
from tests.modules.resources import check_api_post_subject_resource_role
from tests.utils import check_api_response


@pytest.mark.parametrize("type,data", [("record", {}), ("extras", [])])
def test_new_template(type, data, api_client, dummy_access_token):
    """Test the "api.new_template" endpoint."""
    response = api_client(dummy_access_token).post(
        url_for("api.new_template"),
        json={"type": type, "identifier": "test", "title": "test", "data": data},
    )

    check_api_response(response, status_code=201)
    assert Template.query.filter_by(identifier="test").first() is not None


def test_add_template_user_role(
    api_client, dummy_access_token, dummy_template, new_user
):
    """Test the "api.add_template_user_role" endpoint."""
    check_api_post_subject_resource_role(
        api_client(dummy_access_token),
        url_for("api.add_template_user_role", id=dummy_template.id),
        new_user(),
        dummy_template,
    )


def test_add_template_group_role(
    api_client, dummy_access_token, dummy_template, dummy_group
):
    """Test the "api.add_template_group_role" endpoint."""
    check_api_post_subject_resource_role(
        api_client(dummy_access_token),
        url_for("api.add_template_group_role", id=dummy_template.id),
        dummy_group,
        dummy_template,
    )
