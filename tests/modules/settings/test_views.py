# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.api.models import AccessToken
from kadi.lib.web import url_for
from tests.utils import check_view_response


def test_edit_profile(client, dummy_user, user_session):
    """Test the "settings.edit_profile" endpoint."""
    with user_session():
        response = client.post(url_for("settings.edit_profile", action="confirm_email"))
        check_view_response(response)

        response = client.get(url_for("settings.edit_profile"))
        check_view_response(response)

        new_email = "test@example.com"
        dummy_user.identity.email_confirmed = True
        response = client.post(
            url_for("settings.edit_profile", action="edit_profile"),
            data={"email": new_email},
        )

        check_view_response(response, status_code=302)
        assert dummy_user.identity.email == new_email
        assert not dummy_user.identity.email_confirmed


def test_change_password(client, dummy_user, user_session):
    """Test the "settings.change_password" endpoint."""
    endpoint = url_for("settings.change_password")

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(
            endpoint,
            data={
                "password": dummy_user.identity.username,
                "new_password": "test1234",
                "new_password2": "test1234",
            },
        )

        check_view_response(response, status_code=302)
        assert dummy_user.identity.check_password("test1234")


def test_manage_tokens(client, user_session):
    """Test the "settings.manage_tokens" endpoint."""
    endpoint = url_for("settings.manage_tokens")

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(endpoint, data={"name": "test"})

        check_view_response(response)
        assert AccessToken.query.filter_by(name="test").first() is not None


def test_manage_trash(client, user_session):
    """Test the "settings.manage_trash" endpoint."""
    with user_session():
        response = client.get(url_for("settings.manage_trash"))
        check_view_response(response)
