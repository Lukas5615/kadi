# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from flask import current_app

from kadi.modules.collections.core import delete_collection
from kadi.modules.collections.models import Collection
from kadi.modules.groups.core import delete_group
from kadi.modules.groups.models import Group
from kadi.modules.main.tasks import clean_resources
from kadi.modules.records.core import delete_record
from kadi.modules.records.models import Record


def test_clean_resources(
    monkeypatch, dummy_collection, dummy_group, dummy_record, user_context
):
    """Test if cleaning resources works correctly."""
    monkeypatch.setitem(current_app.config, "RESOURCES_MAX_AGE", 0)

    with user_context():
        delete_record(dummy_record)
        delete_collection(dummy_collection)
        delete_group(dummy_group)

        clean_resources()

        assert not Record.query.all()
        assert not Collection.query.all()
        assert not Group.query.all()
