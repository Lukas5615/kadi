# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.resources.utils import add_link
from kadi.lib.web import url_for
from tests.modules.resources import check_api_delete_subject_resource_role
from tests.utils import check_api_response


def test_delete_collection(api_client, dummy_access_token, dummy_collection):
    """Test the "api.delete_collection" endpoint."""
    response = api_client(dummy_access_token).delete(
        url_for("api.delete_collection", id=dummy_collection.id)
    )

    check_api_response(response, status_code=204)
    assert dummy_collection.state == "deleted"


def test_remove_collection_record(
    api_client, dummy_access_token, dummy_collection, dummy_record, dummy_user
):
    """Test the "api.remove_collection_record" endpoint."""
    add_link(dummy_collection.records, dummy_record, user=dummy_user)

    response = api_client(dummy_access_token).delete(
        url_for(
            "api.remove_collection_record",
            collection_id=dummy_collection.id,
            record_id=dummy_record.id,
        )
    )

    check_api_response(response, status_code=204)
    assert not dummy_collection.records.all()


def test_remove_collection_user_role(
    api_client, dummy_access_token, dummy_collection, dummy_user, new_user
):
    """Test the "api.remove_collection_user_role" endpoint."""
    user = new_user()

    check_api_delete_subject_resource_role(
        api_client(dummy_access_token),
        url_for(
            "api.remove_collection_user_role",
            collection_id=dummy_collection.id,
            user_id=user.id,
        ),
        user,
        dummy_collection,
        remove_creator_endpoint=url_for(
            "api.remove_collection_user_role",
            collection_id=dummy_collection.id,
            user_id=dummy_user.id,
        ),
    )


def test_remove_collection_group_role(
    api_client, dummy_access_token, dummy_collection, dummy_group
):
    """Test the "api.remove_collection_group_role" endpoint."""
    check_api_delete_subject_resource_role(
        api_client(dummy_access_token),
        url_for(
            "api.remove_collection_group_role",
            collection_id=dummy_collection.id,
            group_id=dummy_group.id,
        ),
        dummy_group,
        dummy_collection,
    )
