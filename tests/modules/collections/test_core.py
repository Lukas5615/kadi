# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.resources.utils import add_link
from kadi.lib.tags.models import Tag
from kadi.modules.collections.core import create_collection
from kadi.modules.collections.core import delete_collection
from kadi.modules.collections.core import purge_collection
from kadi.modules.collections.core import restore_collection
from kadi.modules.collections.core import update_collection
from kadi.modules.collections.models import Collection
from kadi.modules.permissions.utils import add_role


def test_create_collection(dummy_user):
    """Test if collections are created correctly."""
    collection = create_collection(
        creator=dummy_user,
        identifier="test",
        title="test",
        description="# test",
        tags=["test"],
        visibility="private",
    )

    assert Collection.query.filter_by(identifier="test").first().id == collection.id
    assert collection.plain_description == "test"
    assert collection.revisions.count() == 1
    assert Tag.query.filter_by(name="test").first() is not None


def test_update_collection(dummy_collection, user_context):
    """Test if collections are updated correctly."""
    with user_context():
        update_collection(dummy_collection, description="# test", tags=["test"])

        assert dummy_collection.plain_description == "test"
        assert dummy_collection.revisions.count() == 2
        assert Tag.query.filter_by(name="test").first() is not None


def test_delete_collection(dummy_collection, user_context):
    """Test if collections are deleted correctly."""
    with user_context():
        delete_collection(dummy_collection)

        assert dummy_collection.state == "deleted"
        assert dummy_collection.revisions.count() == 2


def test_restore_collection(dummy_collection, user_context):
    """Test if collections are restored correctly."""
    with user_context():
        delete_collection(dummy_collection)
        restore_collection(dummy_collection)

        assert dummy_collection.state == "active"
        assert dummy_collection.revisions.count() == 3


def test_purge_collection(
    db, dummy_collection, dummy_group, dummy_record, new_user, user_context
):
    """Test if collections are purged correctly."""
    with user_context():
        user = new_user()

        add_link(dummy_collection.records, dummy_record)
        add_role(user, "collection", dummy_collection.id, "member")

        purge_collection(dummy_collection)
        db.session.commit()

        assert Collection.query.get(dummy_collection.id) is None
        # Only the system role should remain.
        assert user.roles.count() == 1
