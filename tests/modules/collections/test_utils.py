# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest

from kadi.modules.collections.utils import get_export_data


@pytest.mark.parametrize("export_type", [None, "json"])
def test_get_export_data(export_type, dummy_collection, user_context):
    """Test if collections are exported correctly."""
    with user_context():
        assert get_export_data(dummy_collection, export_type=export_type) is not None
