# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import json
from uuid import uuid4

from flask_login import login_user as _login_user

from .schemas import UserSchema
from kadi.lib.storage.misc import delete_thumbnail
from kadi.lib.storage.misc import save_as_thumbnail


def login_user(identity, **kwargs):
    r"""Login a user by their identity.

    Wraps Flask-Login's ``login_user`` function but also makes sure to update the user's
    current identity.

    :param identity: The identity to log in with.
    :return: ``True`` if the login was successful, ``False`` otherwise.
    """
    user = identity.user
    user.identity = identity

    return _login_user(user, force=True)


def save_user_image(user, file_object):
    """Set an image file as a user's profile image.

    Uses :func:`kadi.lib.storage.local.save_as_thumbnail` to create and save a thumbnail
    of the given image. If the image cannot be saved, :func:`delete_user_image` will be
    called.

    :param user: The user to set the new profile image for.
    :param file_object: The image file.
    """
    user.image_name = uuid4()
    if not save_as_thumbnail(str(user.image_name), file_object):
        delete_user_image(user)


def delete_user_image(user):
    """Delete a user's profile image if one exists.

    Uses :func:`kadi.lib.storage.local.delete_thumbnail` to delete the actual thumbnail
    file.

    :param user: The user whose profile image should be deleted.
    """
    if user.image_name:
        delete_thumbnail(str(user.image_name))
        user.image_name = None


def json_user(user):
    """Convert a user into a JSON representation.

    :param user: The user to convert.
    :return: The converted user.
    """
    schema = UserSchema(_internal=True, exclude=["about"])
    return json.dumps(schema.dump(user), separators=(",", ":"))
