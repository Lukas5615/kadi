Models
======

This section describes all database models found in the application. Since this
application uses `SQLAlchemy <https://www.sqlalchemy.org>`__ as an ORM, all
database models have corresponding Python classes.

General
-------

.. automodule:: kadi.lib.api.models
   :members:
   :show-inheritance:

.. automodule:: kadi.lib.revisions.models
   :members:
   :show-inheritance:

.. automodule:: kadi.lib.tags.models
   :members:
   :show-inheritance:

.. automodule:: kadi.lib.tasks.models
   :members:
   :show-inheritance:

Modules
-------

.. automodule:: kadi.modules.accounts.models
   :members:
   :show-inheritance:

.. automodule:: kadi.modules.collections.models
   :members:
   :show-inheritance:

.. automodule:: kadi.modules.groups.models
   :members:
   :show-inheritance:

.. automodule:: kadi.modules.notifications.models
   :members:
   :show-inheritance:

.. automodule:: kadi.modules.permissions.models
   :members:
   :show-inheritance:

.. automodule:: kadi.modules.records.models
   :members:
   :show-inheritance:

.. automodule:: kadi.modules.templates.models
   :members:
   :show-inheritance:
