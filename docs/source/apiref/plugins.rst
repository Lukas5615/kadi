.. _apiref-plugins:

Plugins
=======

This section contains all API references of the :mod:`kadi.plugin` package that
were not already listed in other sections.

.. automodule:: kadi.plugins.utils
   :members:
   :show-inheritance:
