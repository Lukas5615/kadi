Project structure
=================

This section is intended to give a brief overview about the general structure
of the project. Specifically, the focus lies on everything contained inside the
``kadi`` directory.

.. _apiref-project_structure-assets:

assets
------

This directory is not a Python package, but instead contains all frontend code,
namely JavaScript code (not including smaller scripts written directly in some
HTML templates) and stylesheets, found in the ``scripts`` and ``styles``
directories respectively, as well as some related configuration files.

Most code inside the ``scripts`` directory is written in plain old (ES6)
JavaScript that is either used globally or only in certain pages. All files
using the ``.vue`` file extension instead contain `Vue.js
<https://vuejs.org>`__ components, which can be used to build custom, reusable
HTML tags.

The ``styles`` directory contains all stylesheets used by the application.
Those can be either normal CSS files or ``.scss`` files using the `Sass
<https://sass-lang.com/>`__ language.

The configuration files are used to install all needed frontend dependencies
and to compile and bundle the sources. The dependencies are listed in
:file:`package.json`, which is used by `npm <https://www.npmjs.com>`__, while
the compilation is done using `webpack <https://webpack.js.org/>`__ as
specified in :file:`webpack.config.js`. Essentially, all files that or not
either plain JavaScript or CSS files will be compiled into code executable by
the browser. Furthermore, in a production environment those files will be
minified as well. All resulting files will be put into the ``kadi/static/dist``
directory where they can be served directly by a web server.

.. _apiref-project_structure-cli:

cli
---

This directory is a Python package that contains code relevant for the command
line interface. The command line interface provides multiple tools inside
different subcommands that can be utilized by using the ``kadi`` command, which
is available after installing the package. This is done by using the
``console_scripts`` entry point defined in :file:`setup.py` as provided by
setuptools. For building the actual interface, the `Click
<https://click.palletsprojects.com/en/7.x/>`__ library is used.

ext
---

This directory is a Python package that contains code relevant for all
extensions used in the application. Most of them are Flask extensions, which
are specifically made to be used inside an existing Flask application. They are
often just wrappers over other libraries, making the integration into a web
application context easier. For each extension, a global object is created that
can be used throughout the application. Generally, this is made possible by
storing all configuration values needed for an extension to do its work in the
current application object.

lib
---

This directory is a Python package that contains code that provides general or
common functionality as well as miscellaneous helper functions.

See also :ref:`Lib <apiref-lib>`.

.. _apiref-project_structure-migrations:

migrations
----------

This directory is not a Python package, but instead contains multiple scripts
and configuration files relevant for running database schema migrations using
`Alembic <https://alembic.sqlalchemy.org/en/latest/>`__. Schema migrations help
managing incremental changes to a relational database schema. This way, each
change can be checked into version control in form of a migration script, which
also allows another developer to run the script and get the same changes to
their development database. Note that once a database is filled with actual
data, this data possibly needs to be migrated as well.

The ``versions`` directory contains the actual incremental migration scripts,
ordered by their date of creation. Each script has an upgrade and downgrade
function, to apply or revert changes to a database schema respectively.

modules
-------

This directory is a Python package that structures code and templates relating
together into different *modules*. This is not to be confused with Python
modules, i.e. a single file containing Python code.

See also :ref:`Modules <apiref-modules>`.

plugins
-------

This directory is a Python package that contains code used for writing plugins.

See also :ref:`Plugins <apiref-plugins>`.

static
------

This directory is not a Python package, but instead contains static files that
can be served directly by a web server, like scripts and stylesheets (once they
are compiled) or images and fonts.

templates
---------

This directory is not a Python package, but instead contains different HTML
templates, snippets and macros that are used in multiple places instead of just
in a single module.
