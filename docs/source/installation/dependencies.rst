Installing the dependencies
---------------------------

Python and Virtualenv
~~~~~~~~~~~~~~~~~~~~~

As the backend code of the application is based on `Flask
<https://flask.palletsprojects.com/en/1.1.x/>`__ and multiple other Python
libraries, Python needs to be installed. Note that Python >=3.6 is required:

.. code-block:: bash

  sudo apt install python3

To create an isolated environment for the application and its dependencies, it
is highly recommended to use `Virtualenv
<https://virtualenv.pypa.io/en/latest/>`__, which can be installed like this:

.. code-block:: bash

  sudo apt install virtualenv

The rest of the documentation assumes virtualenv is installed.

Libraries
~~~~~~~~~

Some libraries used in the project depend on external libraries themselves.
Those should normally be installed already, but may have to be installed
seperately in some cases. The following install command lists all of those
libraries:

.. code-block:: bash

  sudo apt install libmagic1

Postgres
~~~~~~~~

The RDBMS used in the application is `PostgresQL
<https://www.postgresql.org/>`__ 11, which can be installed like this:

.. code-block:: bash

  sudo apt install postgresql

Elasticsearch
~~~~~~~~~~~~~

`Elasticsearch <https://www.elastic.co/products/elasticsearch>`__ is the
full-text search engine used in the application. Currently only version 7 is
supported, which can be installed like this:

.. code-block:: bash

  sudo apt install wget apt-transport-https gnupg
  wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
  echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-7.x.list
  sudo apt update && sudo apt install elasticsearch

To start Elasticsearch and also configure it to start automatically when the
system boots, the following commands can be used:

.. code-block:: bash

  sudo systemctl enable elasticsearch.service
  sudo systemctl start elasticsearch.service

Redis
~~~~~

`Redis <https://redis.io/>`__ is an in-memory data structure that can be used
for different purposes. Currently it is used as cache for rate limits and as a
message broker for running asynchronous tasks with `Celery
<https://docs.celeryproject.org/en/stable/>`__, a distributed task queue. It
can be installed like this:

.. code-block:: bash

  sudo apt install redis-server
