Manual installation
===================

Manual installation is recommended for first-time installations, as all
necessary steps are explained in more detail. The installation steps are
supposed to be run on a freshly installed server or virtual machine running a
current version of Debian or Ubuntu.

.. include:: ../dependencies.rst

uWSGI
~~~~~

`uWSGI <https://uwsgi-docs.readthedocs.io/en/latest/>`__ is an application
server implementing the WSGI interface, used to serve the actual Python
application. It can be installed like this:

.. code-block:: bash

  sudo apt install uwsgi uwsgi-plugin-python3

The python3 plugin is needed as well, since the distro-supplied uWSGI package
is built in a modular way.

Apache
~~~~~~

The `Apache HTTP Server <https://httpd.apache.org>`__ is used as a reverse
proxy server in front of uWSGI, handling the actual HTTP requests. The server
as well as all additionally required modules can be installed like this:

.. code-block:: bash

  sudo apt install apache2 libapache2-mod-proxy-uwsgi libapache2-mod-xsendfile

Installing Kadi4Mat
-------------------

Before installing the application, it is best to create a dedicated user that
the application and all required services will run under:

.. code-block:: bash

  sudo adduser kadi --system --home /opt/kadi --ingroup www-data --shell /bin/bash

As some later steps require root privileges again, all commands that require
the newly created user are prefixed with ``kadi $``. Therefore it is best to
switch to the new user using a separate terminal window:

.. code-block:: bash

  sudo su - kadi

To create and activate a new virtual environment for the application, the
following commands can be used:

.. code-block:: bash

  kadi $ virtualenv -p python3 ${HOME}/venv
  kadi $ source ${HOME}/venv/bin/activate

This will create and activate a new virtual environment named *venv* using
Python 3 as interpreter. For all following steps requiring the newly created
user, the virtual environment is assumed to be active. Afterwards, the
application can be installed like this:

.. code-block:: bash

  kadi $ pip install kadi

Configuration
-------------

Postgres
~~~~~~~~

To set up Postgres, a user and a database belonging to that user have to be
created:

.. code-block:: bash

  sudo -u postgres createuser -P kadi
  sudo -u postgres createdb -O kadi kadi -E utf-8

.. _installation-production-configuration-kadi4mat:

Kadi4Mat
~~~~~~~~

While most of the application configuration values have usable defaults set,
some values need to be set explicitly when using a production environment. For
this, a separate configuration file has to be created, for example:

.. code-block:: bash

  kadi $ mkdir ${HOME}/config
  kadi $ touch ${HOME}/config/kadi.py
  kadi $ chmod 640 ${HOME}/config/kadi.py

This file can be treated like a normal Python file, i.e. calculating values or
importing other modules will work, however, the syntactical rules and
formatting of Python also apply. It is important for all services that need
access to the application configuration, but also for using the Kadi command
line interface (CLI). The Kadi CLI offers some useful tools and utility
functions running in the context of the application (see also :ref:`Command
line interfaces <development-general-cli>`). As such, it also needs access to
the configuration file, which can be done setting the appropriate environment
variable:

.. code-block:: bash

  kadi $ export KADI_CONFIG_FILE=${HOME}/config/kadi.py

The above line could also be added to :file:`.profile` or a similar
configuration file for convenience, so it is always executed when switching to
the new user, together with activating the virtual environment:

.. code-block:: bash

  kadi $ echo 'export KADI_CONFIG_FILE=${HOME}/config/kadi.py' >> ~/.profile
  kadi $ echo 'test -z "${VIRTUAL_ENV}" && source ${HOME}/venv/bin/activate' >> ~/.profile

Shown below is an example of such a configuration file:

.. code-block:: python3

    import kadi.lib.constants as const
    AUTH_PROVIDERS = [{"type": "local", "allow_registration": True}]
    FOOTER_NAV_ITEMS = [
        ("https://example.com/legals", {"en": "Legals", "de": "Impressum"}),
        ("https://example.com/privacy", {"en": "Privacy policy", "de": "Datenschutz"}),
    ]
    MAIL_ERROR_LOGS = ["admin@kadi4mat.edu"]
    MAIL_NO_REPLY = "no-reply@kadi4mat.edu"
    MAX_UPLOAD_SIZE = const.ONE_GB
    MAX_UPLOAD_USER_QUOTA = 10 * const.ONE_GB
    MISC_UPLOADS_PATH = "/opt/kadi/uploads"
    SECRET_KEY = "<secret_key>"
    SENTRY_DSN = None
    SERVER_NAME = "kadi4mat.edu"
    SMTP_HOST = "smtp.kadi4mat.edu"
    SMTP_PASSWORD = ""
    SMTP_PORT = 25
    SMTP_USE_TLS = False
    SMTP_USERNAME = ""
    SQLALCHEMY_DATABASE_URI = "postgresql://kadi:<password>@localhost/kadi"
    STORAGE_PATH = "/opt/kadi/storage"

The example shows the most important configuration values that may need to be
set or changed from the defaults, which are briefly described below:

AUTH_PROVIDERS
  This configuration value specifies the authentication providers to be used in
  the application. Each entry at least requires specifying an authentication
  provider type. All other configuration values differ for each provider, e.g.
  the local provider offers an option to enable registration of users.

FOOTER_NAV_ITEMS
  This configuration value allows to specify additional items for the
  navigation footer without the need to write a separate plugin. Besides the
  URL, a title for the navigation item can be provided in different languages.
  The default language always has to be provided as it will be used as
  fallback.

MAIL_ERROR_LOGS
  This setting allows to configure a list of email addresses which will receive
  logs of unexpected errors and exceptions using Python's ``SMTPHandler``. As
  this will send an email for each error instance, setting up Sentry should be
  the preferred way for error monitoring (see ``SENTRY_DSN``).

MAIL_NO_REPLY
  The email address that will be used to send no-reply emails from. Defaults to
  ``"no-reply@<fqdn>"``, where ``<fqdn>`` is the fully qualified domain name of
  the server.

MAX_UPLOAD_*
  These values specify the maximum size of a single upload
  (``MAX_UPLOAD_SIZE``) and the total amount of data
  (``MAX_UPLOAD_USER_QUOTA``) a user can upload. If not set, the values default
  to 1GB and 10GB respectively. Setting them to ``None`` will remove the limits
  altogether. Importing the ``constants`` module as seen in the very first line
  of the example configuration allows us to use some convenient constants for
  the file sizes as well.

MISC_UPLOADS_PATH
  Specifies the local path that all general user uploads will be stored in
  (e.g. profile or group pictures). Permissions for this directory need to be
  set accordingly, e.g.:

  .. code-block:: bash

    kadi $ chown kadi:www-data /opt/kadi/uploads
    kadi $ chmod 750 /opt/kadi/uploads

SECRET_KEY
  The secret key is used for anything that requires encryption in the
  application itself (e.g. the Flask session), so it should be an appropriately
  secure value. The Kadi CLI also offers a command to generate such a secret
  key:

  .. code-block:: bash

    kadi utils secret-key

SENTRY_DSN
  A Sentry DSN (Data Source Name) which can be used to integrate the `Sentry
  <https://sentry.io/>`__ error monitoring tool with the application.

SERVER_NAME
  The name or IP of the host.

SQLALCHEMY_DATABASE_URI
  This value specifies the database connection to use. The password has to be
  set according to the value used for setting up the database user.

SMTP_*
  These values specify the SMTP connection needed to send emails, most
  importantly the ``SMTP_HOST`` and the ``SMTP_PORT``. If the SMTP server
  requires authentication, the username and password can be specified using
  ``SMTP_USERNAME`` and ``SMTP_PASSWORD``. If ``SMTP_USE_TLS`` is enabled,
  STARTTLS will be used for an encrypted SMTP connection.

STORAGE_PATH
  Specifies the path that all local files and uploads of records will be stored
  in. Permissions for this directory need to be set accordingly, e.g.:

  .. code-block:: bash

    kadi $ chown kadi:www-data /opt/kadi/storage
    kadi $ chmod 750 /opt/kadi/storage

uWSGI
~~~~~

To generate a basic configuration for uWSGI, the Kadi CLI can be used:

.. code-block:: bash

  kadi $ kadi utils uwsgi --out kadi.ini

The generated configuration should be rechecked as further customization may be
necessary. Once the configuration is suitable, it can be enabled like this:

.. code-block:: bash

  sudo mv /opt/kadi/kadi.ini /etc/uwsgi/apps-available/
  sudo ln -s /etc/uwsgi/apps-available/kadi.ini /etc/uwsgi/apps-enabled/

Apache
~~~~~~

To generate a basic configuration for Apache, the Kadi CLI can be used:

.. code-block:: bash

  kadi $ kadi utils apache --out kadi.conf

The command will ask, among others, for a certificate and a key file, used to
encrypt the HTTP traffic using SSL/TLS (HTTPS). For testing or internal usage,
a self signed certificate may be used, which can be generated like this:

.. code-block:: bash

  sudo openssl req -x509 -nodes -days 365 -newkey rsa:4096 -keyout /etc/ssl/private/kadi.key -out /etc/ssl/certs/kadi.crt -subj "/CN=<server_name>" -addext "subjectAltName=DNS:<server_name>"

The server name placeholder has to be substituted with the actual name or IP of
the host.

.. note::

  Using a self signed certificate will lead to warnings when accessing the
  application through the web browser. For actual production use, a real
  certificate signed by a public certificate authority should be used.

Alternatively, a real certificate may be obtained from `Let's Encrypt
<https://letsencrypt.org/>`__ for free. This is easiest done using their
`Certbot <https://certbot.eff.org/>`__ utility. For this to work, the server
needs to be reachable externally on port 80.

The generated configuration should be rechecked as further customization may be
necessary. Once the configuration is suitable, it can be enabled like this:

.. code-block:: bash

  sudo mv /opt/kadi/kadi.conf /etc/apache2/sites-available/
  sudo a2dissite 000-default
  sudo a2ensite kadi

Finally, all additionally required Apache modules have to be activated as well:

.. code-block:: bash

  sudo a2enmod proxy_uwsgi ssl xsendfile headers

Celery
~~~~~~

To run Celery as a background service, it is recommended to set it up as a
systemd service. To generate the necessary unit file, the Kadi CLI can be used
again:

.. code-block:: bash

  kadi $ kadi utils celery --out kadi-celery.service

Celery beat, used to execute periodic tasks, needs its own unit file as well:

.. code-block:: bash

  kadi $ kadi utils celerybeat --out kadi-celerybeat.service

The generated configurations should be rechecked as further customization may
be necessary. Once both configurations are suitable, they can be enabled like
this:

.. code-block:: bash

  sudo mv /opt/kadi/kadi-celery.service /etc/systemd/system/
  sudo mv /opt/kadi/kadi-celerybeat.service /etc/systemd/system/

The services need access to the ``/var/log/celery`` directory to store logs,
which has to be created with the appropriate permissions:

.. code-block:: bash

  sudo mkdir /var/log/celery
  sudo chown -R kadi:www-data /var/log/celery

The user and group need to be set depending on what values were chosen in the
unit file.

To store temporary files, access to the ``/var/run/celery`` directory is needed
as well. Since all directories in ``/var/run/`` are ephemeral, they have to be
recreated on each reboot. To do so, an appropriate configuration has to be
created in ``/usr/lib/tmpfiles.d``:

.. code-block:: bash

  echo "d /run/celery 0755 kadi www-data" | sudo tee /usr/lib/tmpfiles.d/celery.conf > /dev/null
  sudo systemd-tmpfiles --create

Again, the user and group need to be set appropriately.

To let systemd know about the new services and also configure them to start
automatically when the system boots, the following commands can be used:

.. code-block:: bash

  sudo systemctl daemon-reload
  sudo systemctl enable kadi-celery kadi-celerybeat

Setting up the application
--------------------------

Before the application can be used, some initialization steps have to be done
using the Kadi CLI again:

.. code-block:: bash

  kadi $ kadi db init     # Initialize the database
  kadi $ kadi search init # Initialize the search indices

Finally, all new and modified services have to be restarted:

.. code-block:: bash

  sudo systemctl restart apache2 uwsgi kadi-celery kadi-celerybeat
