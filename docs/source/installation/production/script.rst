Installation via setup script
=============================

The setup script to install Kadi4Mat can always be found on GitLab, the latest
version of it is available `here
<https://gitlab.com/iam-cms/kadi/-/blob/master/bin/setup.sh>`__. It is supposed
to be run on a freshly installed server or virtual machine running a current
version of Debian or Ubuntu. The following commands can be used to easily run
the script from the command line:

.. code-block:: bash

  sudo apt install curl
  curl https://gitlab.com/iam-cms/kadi/-/raw/master/bin/setup.sh > kadi_setup.sh
  less kadi_setup.sh # Let's get an overview about what it does before running it as root
  chmod +x kadi_setup.sh
  sudo ./kadi_setup.sh

The script should mostly run through autonomously, prompting for input at some
steps where necessary. Once it is complete, some further post-installation
steps may be required, which the script will notify about.
