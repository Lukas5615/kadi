General
=======

This section describes some generally useful information for development.
Before reading, make sure you have a working :ref:`development environment
<installation-development>` and the correct virtual environment active.

.. _development-general-tools:

Tools
-----

EditorConfig
~~~~~~~~~~~~

For general editor settings related to indentation, maximum line length and
line endings, the settings in the :file:`.editorconfig` file should be applied.
The file can be used in combination with a text editor or IDE that supports it.
For more information, take a look at the `EditorConfig
<https://editorconfig.org/>`__ documentation.

pre-commit
~~~~~~~~~~

`pre-commit <https://pre-commit.com/>`__ is a framework for managing and
maintaining multi-language pre-commit hooks, which get executed each time
``git commit`` is run. pre-commit should be already installed. The hooks listed
in :file:`.pre-commit-config.yaml` can be installed by simply running:

.. code-block:: bash

  pre-commit install

The hooks can also be run manually using:

.. code-block:: bash

  pre-commit run -a

This will run all pre-commit hooks on all versioned files.

black
~~~~~

`black <https://black.readthedocs.io/en/stable/>`__ is a code formatter which
is used throughout all Python code in the project. black should be already
installed and can be applied on one or multiple files using:

.. code-block:: bash

  black <path>

Besides running black on the command line, there are also various `integrations
<https://black.readthedocs.io/en/stable/editor_integration.html>`__ available
for different text editors and IDEs.

black is also part of the pre-commit hooks. As such, it will run automatically
on each commit or when running the pre-commit hooks manually.

Pylint
~~~~~~

`Pylint <https://www.pylint.org/>`__ is a static code analysis tool for Python
and should already be installed as well. It can be used on the command line to
aid with detecting some common programming or style guide mistakes, even if not
using an IDE that already does that. It can be used for the whole ``kadi``
package by running the following:

.. code-block:: bash

  pylint kadi --disable=fixme

Pylint will use the configuration specified in the :file:`.pylintrc` file in
the application's root directory. The most interesting value in there is the
``disable`` key found in the ``[MESSAGES CONTROL]`` section, which suppresses
some messages that we generally propably do not need to care about. This list
can be extended as seen in the example call above, which would exclude all
messages about "TODO"-comments. The inverse operation can be achieved by using
the ``--enable`` option.

Some modules also contain code that should never be checked for certain things.
Using certain comments, one can instruct Pylint to skip such code, e.g. the
following line will not raise a message for an unused import statement:

.. code-block:: python3

  import something # pylint: disable=unused-import

ESLint
~~~~~~

`ESLint <https://eslint.org/>`__ is a linter which is used for all JavaScript
code throughout the project, including any code snippets inside script tags and
Vue components. ESLint should be already installed and can be applied on one or
multiple files using:

.. code-block:: bash

  npx eslint <path> --ext .js,.vue,.html

Per default, ESLint will only run on JavaScript files, which is why the
``--ext`` parameter is used here. Aside from linting, ESLint will also check
some basic code formatting rules. Many of them can be applied automatically
running ESLint again with the ``--fix`` parameter. Keep in mind that this does
not always work properly, especially for JavaScript code inside script tags.

The configuration of ESlint can be found inside :file:`.eslintrc.js`. Besides
running ESlint on the command line, there are also various `integrations
<https://eslint.org/docs/user-guide/integrations>`__ available for different
text editors and IDEs.

Some files also contain code that should never be checked for certain things.
Using certain comments, one can instruct ESLint to skip such code, e.g. the
following will suppress errors for unused variables in the specified function:

.. code-block:: js

  /* eslint-disable no-unused-vars */
  function foo(a) {}
  /* eslint-enable no-unused-vars */

.. code-block:: js

  // eslint-disable-next-line no-unused-vars
  function foo(a) {}

ESLint is also part of the pre-commit hooks. As such, it will run automatically
on each commit or when running the pre-commit hooks manually.

virtualenvwrapper
~~~~~~~~~~~~~~~~~

`virtualenvwrapper <https://virtualenvwrapper.readthedocs.io/en/latest/>`__ is
an extension to the virtualenv tool and can be used to manage and switch
between multiple virtual environments more easily.

The tool needs to be installed globally, for example by running:

.. code-block:: bash

  sudo apt install virtualenvwrapper

Or using pip3 outside any virtual environment, which will generally install a
newer version:

.. code-block:: bash

  pip3 install virtualenvwrapper

Afterwards, some environment variables have to be set. Generally, one suitable
place for them would be the `.bashrc` file. An example could look like the
following:

.. code-block:: bash

  export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
  export WORKON_HOME=$HOME/.venvs
  source $HOME/.local/bin/virtualenvwrapper.sh

Please refer to the official documentation about their meaning as well as other
possible variables that can be used, as their values differ by system and
personal preferences.

.. _development-general-cli:

Command line interfaces
-----------------------

There are two command line interfaces for running different tools or utility
functions. These can be used running the following commands:

.. code-block:: bash

  flask # Flask CLI
  kadi  # Kadi CLI

Running them will give a list of all available subcommands. The first CLI comes
from Flask itself (see `Command Line Interface
<https://flask.palletsprojects.com/en/1.1.x/cli/>`__ and also `Working with the
Shell <https://flask.palletsprojects.com/en/1.1.x/shell/>`__) and also includes
commands added by Flask extensions, while the Kadi CLI contains all
application-specific commands (see also :ref:`cli
<apiref-project_structure-cli>`).

The Kadi CLI also ensures that each command runs inside an application context,
i.e. all commands will have access to the application's configuration, which
can be modified as usual by changing the environment or overriding default
configuration values (see :ref:`Configuring Kadi4Mat
<installation-development-configuration-kadi4mat>`). For this reason, some
(sub)commands are simply wrappers over existing ones, making their use easier
in certain scenarios or environments.

Scripts
-------

Some additional utility scripts that are not part of the CLI can be found in
the :file:`bin` directory in the project's root directory. Generally, each
script should offer a helpful description when running it with ``--help``, for
example:

.. code-block:: bash

  bin/apply_license.py --help

The scripts make use of the Click library which may not be present if not
working inside an active virtual environment. In that case, it can also be
installed globally:

.. code-block:: bash

  pip3 install click

Backend
-------

Adjusting or adding database models
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

When adding a new model or adjusting an existing one, a new migration script
has to be created to update the database schema (see also :ref:`migrations
<apiref-project_structure-migrations>`). To automatically generate such a
script, the following command can be used:

.. code-block:: bash

  flask db migrate -m "Add some new table"

The resulting code of the migration script should be checked and adjusted
accordingly. Afterwards, the database can be upgraded by running the following
command:

.. code-block:: bash

  flask db upgrade

Note that when adjusting existing models further steps may be necessary to
potentially migrate any existing data as well.

Frontend
--------

Writing frontend code
~~~~~~~~~~~~~~~~~~~~~

When writing frontend code, it is recommended to run the following in a
separate terminal:

.. code-block:: bash

  kadi assets watch

Alternatively, npm can be used directly as well, which will run the watcher
directly without installing the frontend dependencies first:

.. code-block:: bash

  npm run watch

This way, changes to existing files will be detected and the resulting bundles
will be rebuilt automatically. When adding new files, the command might have to
be restarted to pick them up, depending on which directory the files reside in.

Common issues
-------------

Can't locate revision identified by ...
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This can happen when working on multiple branches with differing commits while
trying to run some database related commands. In this case, one of those
branches is missing one or more database migration scripts/revisions that were
already applied previously. At least one of those scripts should be associated
with the identifier printed in the error message.

The best way to fix it is to simply bring both branches up to date. If this is
not an option for some reason, the database can simply be downgraded again to
the lowest common revision:

.. code-block:: bash

  kadi db downgrade <revision>

Note that we need to be one the branch that actually has the missing migration
scripts to be able to run them. Also, this can potentially erase some data from
the database, depending on the content of the migration scripts.

The Flask dev server and/or the webpack watcher are not working properly
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Both the Flask development server and webpack use inotify to efficiently watch
for any file changes to automatically restart the server/rebuild the asset
bundles. The number of file watches that inotify can use may be limited by the
operating system per default. In that case, the limit can be increased
permanently by running:

.. code-block:: bash

  echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p

This amount should be fine in most cases. However, note that this might use
more (unswappable) kernel memory than before.
