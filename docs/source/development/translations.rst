Translations
============

This section describes different aspects about translating the application,
also referred to as "i18n".

Backend
~~~~~~~

Translations in the backend refer to both backend code itself as well as
server-side rendered templates.

For the backend code, the functions offered by Babel (see also `Flask-Babel
<https://pythonhosted.org/Flask-Babel/>`__) can be used, namely ``gettext``
(usually imported as ``_``), ``lazy_gettext`` (usually imported as ``_l``) and
``ngettext``.

For server-side rendered templates using Jinja, the same functions can be used
as well. Additionally, there is the following special directive provided by the
`i18n Jinja extension
<https://jinja.palletsprojects.com/en/2.11.x/templates/#i18n>`__:

.. code-block:: jinja

  {% trans %}text{% endtrans %}

After marking all translatable strings accordingly, the following command can
be used to update all message catalogs (``*.po`` files) found in the
:file:`translations` directory:

.. code-block:: bash

  kadi i18n update

After translating all strings, the following command can be used to compile the
message catalogs for use in the application:

.. code-block:: bash

  kadi i18n compile

Frontend
~~~~~~~~

Translations in the frontend refer to both frontend code itself as well as
client-side rendered templates and components.

For all frontend translations, the ``i18next`` library is used, which requires
translations in JSON format. The translations can be found inside the
``assets/translations`` directory and also contain the default language, since
the library uses keys for each string to translate, instead of using the
strings to translate directly.

The following example illustrates how a translation can be loaded using a
specified key as well as interpolation:

.. code-block:: js

  i18n.t('warning.removeIdentifier', {identifier: 'foo'})

The respective key (which is a nested key in this case) has to be added
manually to the translation file of the default language. To synchronize the
new key with all other translation files, the following command can be used
instead of doing that step manually as well:

.. code-block:: bash

  kadi i18n sync

This will add any new key from the default translation file to all other
translation files with an empty string as value. Furthermore, all keys will be
sorted alphabetically. Note that keys that are not part of the default
translation file will be removed.
