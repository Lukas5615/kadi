Plugins
=======

This section describes how different aspects of the application can be extended
or overridden using plugins, i.e. without touching the actual application code.

Plugin hooks
------------

The plugin infrastructure builds upon `pluggy
<https://pluggy.readthedocs.io/en/latest/index.html>`__, a generic plugin
system that enables function hooking. In short, plugins can implement different
hook specifications which will be called in certain places in the application
flow. The following hook specifications currently exist:

.. automodule:: kadi.plugins.hookspecs
   :members:
   :show-inheritance:
